Rails.application.routes.draw do
  devise_for :users, controllers: {registrations: 'registrations'}

  root 'categories#index'

  resources :categories, only: [:show, :index] do
    resources :posts do
      resources :comments
    end
  end
end
