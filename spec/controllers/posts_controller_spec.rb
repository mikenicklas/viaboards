require 'rails_helper'

describe PostsController, type: :controller do
  let(:post_attrs) { attributes_for(:post) }
  let(:category) { create(:category) }

  describe '#create' do
    it 'redirects to post path on success' do
      sign_in create(:user)
      post :create, params: {category_id: category.id, post: post_attrs}
      success_path = category_post_path(category_id: category.id, id: Post.last.id)
      expect(controller).to redirect_to(success_path)
    end

    it 'renders new on failure' do
      sign_in create(:user)
      allow_any_instance_of(Post).to receive(:save).and_return false
      post :create, params: {category_id: category.id, post: post_attrs}
      expect(controller).to render_template :new
    end
  end
end
