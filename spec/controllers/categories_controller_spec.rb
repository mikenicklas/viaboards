require 'rails_helper'

describe CategoriesController, type: :controller do

  describe '#show' do
    it 'renders template when id is given' do
      category = create(:category)
      get :show, params: {id: category.id}
      expect(controller).to render_template :show
    end

    it 'redirects to root when category doesnt exist' do
      allow(Category).to receive(:find_by_id).and_return nil
      category = create(:category)
      get :show, params: {id: category.id}
      expect(controller).to redirect_to root_path
    end
  end

end
