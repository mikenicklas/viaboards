require 'rails_helper'

RSpec.feature "UserCanCreateAPosts", type: :feature do
  let(:user) { create(:user) }

  it 'user can sign in and create a post' do
    category = create(:category, name: 'Itinerary')
    visit root_path
    find('.sign-in-btn').click
    within '#sign-in-modal' do
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      click_button 'Sign In'
    end
    click_on "view #{category.name} posts"
    click_on 'New Post'
    fill_in 'post[title]', with: 'Which place in Europe should I go to?'
    fill_in 'post[body]', with: 'Thinking about Europe...where to?'
    click_on 'Create Post'
    expect(page).to have_content 'Comments'
  end
end
