require 'rails_helper'

RSpec.feature "UserCanAddAComments", type: :feature do


  it 'user can add a comment' do
    user = create(:user)
    category = create(:category)
    post = create(:post, category: category, author: user)

    visit root_path
    find('.sign-in-btn').click
    within '#sign-in-modal' do
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      click_button 'Sign In'
    end
    find('.html-link').click
    expect(page).to have_content 'Comments'
    fill_in 'comment[body]', with: 'This is a really good idea'
    click_on 'Add Comment'
    expect(page).to have_content "Commented By: #{user.public_name}"
  end
end
