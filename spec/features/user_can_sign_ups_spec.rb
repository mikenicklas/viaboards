require 'rails_helper'

RSpec.feature "UserCanSignUp", type: :feature do
  it 'user can sign up' do
    visit root_path
    click_on 'Sign Up'
    within '#sign-up-modal' do
      fill_in 'user[email]', with: 'test@example.com'
      fill_in 'user[first_name]', with: 'John'
      fill_in 'user[last_name]', with: 'Doe'
      fill_in 'user[public_name]', with: 'jdoe10'
      fill_in 'user[password]', with: 'testing123'
      fill_in 'user[password_confirmation]', with: 'testing123'
      click_button 'Sign up'
    end
    expect(page).to have_text 'Sign Out'
    expect(User.last.public_name).to eql 'jdoe10'
  end
end
