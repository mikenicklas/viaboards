require 'rails_helper'

describe Post do
  subject { create(:post) }
  it 'belongs to a category' do
    category_assoc = Post.reflect_on_association(:category).macro
    expect(category_assoc).to eql :belongs_to
  end

  it 'belongs to an author' do
    category_assoc = Post.reflect_on_association(:author).macro
    expect(category_assoc).to eql :belongs_to
  end

  it 'is invalid without a title' do
    subject.title = nil
    expect(subject).to be_invalid
  end

  it 'is invalid without a body' do
    subject.body = nil
    expect(subject).to be_invalid
  end
  
end
