require 'rails_helper'

describe Category do
  subject { create(:category) }

  it 'should be invalid without a name' do
    subject.name = nil
    expect(subject).to be_invalid
  end

  it 'should be invalid without a unique name' do
    dup_name = build(:category, name: subject.name)
    expect(dup_name).to be_invalid
  end

end
