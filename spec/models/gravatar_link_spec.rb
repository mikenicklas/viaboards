require 'rails_helper'

describe GravatarLink do
  let(:user) { create(:user) }
  subject { GravatarLink }

  describe '.for_email' do
    it 'returns gravatar link' do
      link = subject.for_email(user.email)
      expect(link).to be_truthy
    end
  end
end
