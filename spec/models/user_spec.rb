require 'rails_helper'

RSpec.describe User, type: :model do
  subject { create(:user) }

  it 'has many posts' do
    post_assoc = User.reflect_on_association(:posts).macro
    expect(post_assoc).to eql :has_many
  end

  it 'is invalid without a first_name' do
    subject.first_name = nil
    expect(subject).to be_invalid
  end

  it 'is invalid without a last_name' do
    subject.last_name = nil
    expect(subject).to be_invalid
  end

  it 'is invalid without a public_name' do
    subject.public_name = nil
    expect(subject).to be_invalid
  end

  it 'should be valid with all attrs' do
    expect(subject).to be_valid
  end

  it 'should be invalid without a unique public_name' do
    dup_public = build(:user, public_name: subject.public_name)
    expect(dup_public).to be_invalid
    dup_public.public_name += 'something-uique'
    expect(dup_public).to be_valid
  end

end
