require 'rails_helper'

RSpec.describe Comment, type: :model do
  subject { create(:comment) }
  
  it 'is invalid without a body' do
    subject.body = nil
    expect(subject).to be_invalid
  end

  it 'should be valid with a body' do
    expect(subject).to be_valid
  end
end
