FactoryGirl.define do
  factory :comment do
    body "This is a really cool post"
    author
    post
  end
end
