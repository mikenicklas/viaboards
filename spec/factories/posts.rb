FactoryGirl.define do
  factory :post do
    title 'A Post Title'
    body 'A Post Body'
    association :author
    association :category
  end
end
