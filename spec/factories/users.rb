FactoryGirl.define do
  sequence(:email) { |n| "testing#{n}@example.com" }
  sequence(:public_name) { |n| "jdoe#{n}" }

  factory :user, aliases: [:author] do
    email
    password 'testing123'
    password_confirmation 'testing123'
    first_name 'john'
    last_name 'doe'
    public_name
  end
end
