require 'digest'

class GravatarLink
  attr_reader :email

  def initialize(email)
    @email = email
  end

  def link
    "https://www.gravatar.com/avatar/#{email_hash}"
  end

  def self.for_email(email)
    new(email).link
  end


  private

  def email_hash
    @email_hash ||= Digest::MD5.hexdigest(email)
  end

end
