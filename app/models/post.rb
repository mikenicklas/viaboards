class Post < ApplicationRecord
  belongs_to :category
  belongs_to :author, class_name: 'User'
  has_many :comments

  validates :title, presence: true
  validates :body, presence: true

  default_scope { order(created_at: :desc) }
  scope :most_recent, -> { order(created_at: :desc).limit(3) }
end
