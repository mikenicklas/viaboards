class Category < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :posts

  def most_recent_posts
    self.posts.most_recent
  end
  
end
