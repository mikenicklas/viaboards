class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts
  has_many :comments

  validates :first_name, :last_name, :public_name, presence: true
  validates :public_name, uniqueness: true

  def gravatar_link
    @gravatar_link ||= GravatarLink.for_email(self.email)
  end
end
