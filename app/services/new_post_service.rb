class NewPostService
  attr_reader :params, :category, :user, :post

  def initialize(user:, params:)
    @params = params
    @category = Category.find_by_id(params[:category_id])
    @user = user
  end

  def call
    @post = Post.new(post_params)
    post.save
  end

  private

  def post_params
    user_and_category = {author_id: user.id, category_id: category.id}
    params.require(:post).permit(:title, :body).merge!(user_and_category)
  end


end
