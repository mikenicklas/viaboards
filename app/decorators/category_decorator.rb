class CategoryDecorator < Draper::Decorator
  delegate_all
  decorates :category

  decorates_association :posts

  def view_all_text
    "view #{model.name} posts"
  end

end
