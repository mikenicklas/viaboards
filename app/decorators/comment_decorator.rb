class CommentDecorator < Draper::Decorator
  delegate_all
  decorates :comment

  def published_by
    "Commented By: #{model.author.public_name}"
  end

end
