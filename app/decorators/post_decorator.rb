class PostDecorator < Draper::Decorator
  delegate_all
  decorates :post

  def published_by
    "Submitted By: #{model.author.public_name}"
  end

  def created_at
    model.created_at.strftime("%m / %e / %y")
  end

end
