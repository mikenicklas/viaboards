class PostsController < ApplicationController
  before_action :authenticate_user!, except: :show

  def new
    @post = Post.new
  end

  def create
    nps = NewPostService.new(user: current_user, params: params)
    if nps.call
      redirect_to category_post_path(category_id: nps.post.category_id, id: nps.post.id)
    else
      render :new
    end
  end

  def show
    @post = PostDecorator.decorate(Post.find(params[:id]))
  end
end
