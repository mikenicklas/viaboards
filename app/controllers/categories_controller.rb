class CategoriesController < ApplicationController

  def index
    @categories = CategoryDecorator.decorate_collection(Category.all)
  end

  def show
    if category = Category.find_by_id(params[:id])
      @category = CategoryDecorator.decorate(category)
    else
      redirect_to root_path
    end
  end

end
