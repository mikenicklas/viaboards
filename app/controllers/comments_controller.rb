class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    post = Post.find(params[:post_id])
    comment = post.comments.new(post_params)
    if comment.save
      flash[:success] = 'Your comment has been successfully added!'
    else
      flash[:danger] = 'Your comment could not be added.'
    end
    redirect_to category_post_path(category_id: post.category.id, id: post.id)
  end

  private

  def post_params
    user_and_post = {author_id: current_user.id, post_id: params[:post_id]}
    params.require(:comment).permit(:body).merge!(user_and_post)
  end

end
