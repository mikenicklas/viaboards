class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.integer :category_id
      t.string :title
      t.text :body
      t.integer :author_id

      t.timestamps
    end
  end
end
