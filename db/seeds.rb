user = User.create!(email: 'testing@example.com',
                    password: 'testing123', password_confirmation: 'testing123',
                    first_name: 'John', last_name: 'Doe', public_name: 'jdoe10')

itinerary = Category.create!(name: 'Itinerary Chat')
first_timers = Category.create(name: 'First Timers')
finding_destination = Category.create(name: 'Finding a Destination')
off_topic = Category.create(name: 'Off-Topic')


10.times do
  itinerary_title = "Need help in #{Faker::Address.country}"
  first_timers_title = "First time in #{Faker::Address.country}...where to eat?"
  destination_title = "Should I go to #{Faker::Address.country} or #{Faker::Address.country}"
  itinerary.posts.create(title: itinerary_title, body: itinerary_title * 10, author_id: user.id)
  first_timers.posts.create(title: first_timers_title, body: first_timers_title * 10, author_id: user.id)
  finding_destination.posts.create(title: destination_title, body: destination_title * 10, author_id: user.id)
  itinerary.posts.create(title: itinerary_title, body: itinerary_title * 10, author_id: user.id)
end
